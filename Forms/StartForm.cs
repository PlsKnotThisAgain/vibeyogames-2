﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Foundation;
using Windows.Storage.Streams;
using VibeYoGames.Properties;

namespace VibeYoGames.Forms {
	public partial class StartForm : Form {

		BluetoothLEAdvertisementWatcher _watcher;
		private LvsDevice _device;
		public StartForm() {
			InitializeComponent();
		}

		private void StartForm_Load(object sender, EventArgs e) {

		}

		private void OnTestOverlayClick(object sender, EventArgs e) {
			var overlay = new SelectionOverlayForm();
			overlay.OnRectSelected += delegate (Rectangle rect) { selectedRectLabel.Text = rect.ToString(); };
			overlay.Show();
		}

		private void FindBtDevice(object sender, EventArgs e) {
			_watcher = new BluetoothLEAdvertisementWatcher();
			btStatus.Text = Enum.GetName(typeof(BluetoothLEAdvertisementWatcherStatus), _watcher.Status);
			_watcher.Received += DeviceFound;
			_watcher.ScanningMode = BluetoothLEScanningMode.Active;
			_watcher.Start();
		}

		private async void DeviceFound(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args) {
			if (args.Advertisement.LocalName.Contains("LVS")) {
				_watcher.Stop();
				SetStatus(Resources.BT_DeviceFound);
				await Task.Delay(20);
				SetStatus(Resources.BT_StopLooking);
				await Task.Delay(20);
				AddStatus(Resources.BT_ConnectAttempt);
				await Task.Delay(20);

				//var bluetoothLeDevice = await BluetoothLEDevice.FromBluetoothAddressAsync(args.BluetoothAddress).AsTask();

				BluetoothLEDevice.FromBluetoothAddressAsync(args.BluetoothAddress).Completed += ConnectedToHush;
				AddStatus(Resources.BT_Connecting);
			} else {
				btStatus.Invoke((MethodInvoker)(() => {
					string line = Resources.BT_Found+" " + args.Advertisement.LocalName + " " + args.AdvertisementType + Environment.NewLine;
					if (btStatus.Text.Contains(line)) return;
					AddStatus(line);
				}));
			}
		}

		private void ConnectedToHush(IAsyncOperation<BluetoothLEDevice> asyncinfo, AsyncStatus asyncstatus) {
			switch (asyncstatus) {
				case AsyncStatus.Canceled:
					AddStatus("Connection cancelled!");
					break;
				case AsyncStatus.Completed:
					var bluetoothLeDevice = asyncinfo.GetResults();
					if (bluetoothLeDevice != null) {
						AddStatus("Successfully connected!");
						_device = new LvsDevice(bluetoothLeDevice, this);
						_device.Vibrate(5, 1000);

					}
					else {
						AddStatus("DEVICE NOT CONNECTED!");
					}
					break;
				case AsyncStatus.Error:
					AddStatus("ERROR WHILE CONNECTING");
					AddStatus(asyncinfo.ErrorCode.ToString());
					break;
				case AsyncStatus.Started:
					AddStatus("Started connecting.");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(asyncstatus), asyncstatus, null);
			}
		}


		public void SetStatus(string status) {
			btStatus.Invoke((MethodInvoker)(() => {
				Logger.LogThisLine(status);
				btStatus.Text = status + Environment.NewLine;
			}));
		}
		public void AddStatus(string status) {
			btStatus.Invoke((MethodInvoker)(() => {
				Logger.LogThisLine(status);
				btStatus.Text += status + Environment.NewLine;
			}));
		}

		private void OnQuit(object sender, FormClosingEventArgs e) {
			_device?.Dispose();
		}
	}
}

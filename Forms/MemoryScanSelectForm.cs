﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VibeYoGames.Forms {
	public partial class MemoryScanSelectForm : Form {

		[StructLayout(LayoutKind.Sequential)]
		private struct MEMORY_BASIC_INFORMATION {
			public IntPtr BaseAddress;
			public IntPtr AllocationBase;
			public AllocationProtect AllocationProtect;
			public IntPtr RegionSize;
			public uint State;
			public AllocationProtect Protect;
			public int Type;
		}



		[StructLayout(LayoutKind.Sequential)]
		public struct MemoryBasicInformation64 {
			public IntPtr BaseAddress;
			public IntPtr AllocationBase;
			public AllocationProtect AllocationProtect;
			public int __alignment1;
			public IntPtr RegionSize;
			public int State;
			public int Protect;
			public int Type;
			public int __alignment2;
		}

		public enum AllocationProtect : uint {
			PageExecute = 0x00000010,
			PageExecuteRead = 0x00000020,
			PageExecuteReadwrite = 0x00000040,
			PageExecuteWritecopy = 0x00000080,
			PageNoaccess = 0x00000001,
			PageReadonly = 0x00000002,
			PageReadwrite = 0x00000004,
			PageWritecopy = 0x00000008,
			PageGuard = 0x00000100,
			PageNocache = 0x00000200,
			PageWritecombine = 0x00000400
		}


		private struct SYSTEM_INFO {
			public ushort processorArchitecture;
			ushort reserved;
			public uint pageSize;
			public IntPtr minimumApplicationAddress;  // minimum address
			public IntPtr maximumApplicationAddress;  // maximum address
			public IntPtr activeProcessorMask;
			public uint numberOfProcessors;
			public uint processorType;
			public uint allocationGranularity;
			public ushort processorLevel;
			public ushort processorRevision;
		}

		[DllImport("kernel32.dll")]
		static extern void GetSystemInfo(out SYSTEM_INFO lpSystemInfo);

		[DllImport("kernel32.dll", SetLastError = true)]
		static extern int VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, out MEMORY_BASIC_INFORMATION lpBuffer, uint dwLength);

		[DllImport("kernel32.dll")]
		public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

		[DllImport("kernel32.dll")]
		public static extern bool ReadProcessMemory(IntPtr hProcess,
			IntPtr lpBaseAddress, byte[] lpBuffer, IntPtr dwSize, ref IntPtr lpNumberOfBytesRead);

		const int ProcessQueryInformation = 0x0400;
		const int MemCommit = 0x00001000;
		const int ProcessWmRead = 0x0010;

		public MemoryScanSelectForm() {
			InitializeComponent();
		}

		private void MemoryScanSelectForm_Load(object sender, EventArgs e) {

		}

		private async void button1_Click(object sender, EventArgs e) {
			button1.Enabled = false;
			//DumpMemory();
			await ScanForShit();
		}

		private async Task ScanForShit() {
			var byteForm = new StringInputForm("Bytes to looks for", "Enter the bytes You're looking for. Use '?' as unknown values.");
			byteForm.ShowDialog();
			if (byteForm.DialogResult == DialogResult.No) {
				byteForm.Dispose();
				return;
			} else if (byteForm.DialogResult == DialogResult.Yes) {
				var byteStr = byteForm.Result.Replace(" ", "").Trim();
				textBox1.Text = "Looking for address " + byteStr;
				Tuple<bool, string[]> x;
				if ((x = (await TryParseToBytes(byteStr))).Item1) {
					var bytes = x.Item2;
					await SearchForData(bytes);
				}
			}
		}

		private Task<Tuple<bool, string[]>> TryParseToBytes(string byteStr) {
			return Task.Run(() => {
				if (byteStr.Length % 2 != 0) {
					return new Tuple<bool, string[]>(false, new string[0]);
				}

				var bytes = new string[byteStr.Length / 2];
				for (int i = 0; i < byteStr.Length; i += 2) {
					if (IsValidChar(byteStr[i]) && IsValidChar(byteStr[i + 1])) {
						bytes[i / 2] = byteStr.Substring(i, 2);
					} else {
						return new Tuple<bool, string[]>(false, new string[0]);
					}
				}
				return new Tuple<bool, string[]>(true, bytes);
			});
		}

		private bool IsValidChar(char c) {
			return c.IsHex() || c == '?';
		}

		private async Task SearchForData(string[] data) {
			GetSystemInfo(out var sysInfo);
			IntPtr procMinAddress = sysInfo.minimumApplicationAddress;
			IntPtr procMaxAddress = sysInfo.maximumApplicationAddress;
			long procMinAddressL = procMinAddress.ToInt64();
			long procMaxAddressL = procMaxAddress.ToInt64();

			var p = Process.GetProcessesByName(textBox2.Text.Trim());
			if (p.Length < 1) {
				button1.Enabled = true;
				return;
			}

			await Task.Delay(1);
			var proc = p[0];
			IntPtr procHandle = OpenProcess(ProcessQueryInformation | ProcessWmRead, false, proc.Id);
			IntPtr bytesRead = new IntPtr(0);
			while (procMinAddressL < procMaxAddressL) {
				VirtualQueryEx(procHandle, procMinAddress, out var memBasicInfo, (uint)Marshal.SizeOf(typeof(MEMORY_BASIC_INFORMATION)));
				if (memBasicInfo.Protect == AllocationProtect.PageReadwrite && memBasicInfo.State == MemCommit) {
					byte[] buffer = new byte[(long)memBasicInfo.RegionSize];
					// read everything in the buffer above
					ReadProcessMemory(procHandle, memBasicInfo.BaseAddress, buffer, memBasicInfo.RegionSize, ref bytesRead);
					// then output this in the file
					for (int i = 0; i < (long)memBasicInfo.RegionSize; i++)
						for (int j = 0; j < data.Length; j++) {
							if (IsCorrectByte(buffer[i + j], data[j])) {
								Debug.Write("Found some shit at "+ (memBasicInfo.BaseAddress + i + j).ToString("X"));
								await Task.Delay(1);
								if (j == data.Length - 1) {
									textBox1.Text = "Found a match:\nStarting address is " + (memBasicInfo.BaseAddress + i).ToString("X") + "\nEnding address is " + (memBasicInfo.BaseAddress + i + j).ToString("X");
									button1.Enabled = true;
									return;

								}
							} else {
								break;
							}
							/*if (((char)buffer[i]).IsAlphanumeric()) {

								sw.WriteLine("0x{0} : {1}", (memBasicInfo.BaseAddress + i).ToString("X"), (char)buffer[i]);
							}*/
						}
				}
				procMinAddressL += (long)memBasicInfo.RegionSize;
				procMinAddress = new IntPtr(procMinAddressL);
			}
			textBox1.Text += "Done!";
			button1.Enabled = true;
		}

		private bool IsCorrectByte(byte b, string s) {
			/*if (b != 0) {
				Debug.WriteLine(s + "||" + Convert.ToString(b & 0xF0, 16).PadLeft(2, '0') + "||" +
				                Convert.ToString(b & 0xF, 16).PadLeft(2, '0'));
			}*/
			string byteString = Convert.ToString(b, 16).PadLeft(2, '0');
			bool firstBit = false;
			firstBit = s[0] == '?' || byteString[0] == s[0];
			bool secondBit = false;
			secondBit = s[1] == '?' || byteString[1] == s[1];
			return firstBit && secondBit;
		}

		private async void DumpMemory() {
			textBox1.Text = "Trying to dump...";
			await Task.Delay(1);
			await DumpMemoryAsync();
		}

		private async Task DumpMemoryAsync() {
			GetSystemInfo(out var sysInfo);
			IntPtr procMinAddress = sysInfo.minimumApplicationAddress;
			IntPtr procMaxAddress = sysInfo.maximumApplicationAddress;
			long procMinAddressL = procMinAddress.ToInt64();
			long procMaxAddressL = procMaxAddress.ToInt64();

			var p = Process.GetProcessesByName(textBox2.Text.Trim());
			if (p.Length < 1) {
				button1.Enabled = true;
				return;
			}

			textBox1.Text = "Scanning...";
			await Task.Delay(1);
			var proc = p[0];
			IntPtr procHandle = OpenProcess(ProcessQueryInformation | ProcessWmRead, false, proc.Id);
			StreamWriter sw = new StreamWriter("dump.txt");

			IntPtr bytesRead = new IntPtr(0);
			while (procMinAddressL < procMaxAddressL) {
				VirtualQueryEx(procHandle, procMinAddress, out var memBasicInfo, (uint)Marshal.SizeOf(typeof(MEMORY_BASIC_INFORMATION)));
				if ((memBasicInfo.Protect == AllocationProtect.PageReadwrite || memBasicInfo.Protect == AllocationProtect.PageReadwrite) && memBasicInfo.State == MemCommit) {
					byte[] buffer = new byte[(long)memBasicInfo.RegionSize];
					// read everything in the buffer above
					ReadProcessMemory(procHandle, memBasicInfo.BaseAddress, buffer, memBasicInfo.RegionSize, ref bytesRead);
					// then output this in the file
					for (int i = 0; i < (long)memBasicInfo.RegionSize; i++)
						if (((char)buffer[i]).IsAlphanumeric() ||true) {
							sw.WriteLine("0x{0} : {1}", (memBasicInfo.BaseAddress + i).ToString("X"), Convert.ToString(buffer[i],16).PadLeft(2,'0'));
						}
				}

				// move to the next memory chunk

				procMinAddressL += (long)memBasicInfo.RegionSize;
				procMinAddress = new IntPtr(procMinAddressL);
			}
			sw.Close();
			textBox1.Text = "Done!";
			button1.Enabled = true;
		}

		private void textBox1_TextChanged(object sender, EventArgs e) {

		}
	}
}

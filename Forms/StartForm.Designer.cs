﻿namespace VibeYoGames.Forms {
	partial class StartForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.testOverlayBtn = new System.Windows.Forms.Button();
			this.selectedRectLabel = new System.Windows.Forms.Label();
			this.lookForBt = new System.Windows.Forms.Button();
			this.btStatus = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// testOverlayBtn
			// 
			this.testOverlayBtn.Location = new System.Drawing.Point(12, 12);
			this.testOverlayBtn.Name = "testOverlayBtn";
			this.testOverlayBtn.Size = new System.Drawing.Size(130, 47);
			this.testOverlayBtn.TabIndex = 0;
			this.testOverlayBtn.Text = "TestOverlay";
			this.testOverlayBtn.UseVisualStyleBackColor = true;
			this.testOverlayBtn.Click += new System.EventHandler(this.OnTestOverlayClick);
			// 
			// selectedRectLabel
			// 
			this.selectedRectLabel.Location = new System.Drawing.Point(148, 12);
			this.selectedRectLabel.MinimumSize = new System.Drawing.Size(130, 47);
			this.selectedRectLabel.Name = "selectedRectLabel";
			this.selectedRectLabel.Size = new System.Drawing.Size(286, 47);
			this.selectedRectLabel.TabIndex = 1;
			this.selectedRectLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lookForBt
			// 
			this.lookForBt.Location = new System.Drawing.Point(12, 65);
			this.lookForBt.Name = "lookForBt";
			this.lookForBt.Size = new System.Drawing.Size(130, 47);
			this.lookForBt.TabIndex = 2;
			this.lookForBt.Text = "Find Bluetooth Device";
			this.lookForBt.UseVisualStyleBackColor = true;
			this.lookForBt.Click += new System.EventHandler(this.FindBtDevice);
			// 
			// btStatus
			// 
			this.btStatus.Location = new System.Drawing.Point(151, 65);
			this.btStatus.Multiline = true;
			this.btStatus.Name = "btStatus";
			this.btStatus.ReadOnly = true;
			this.btStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.btStatus.Size = new System.Drawing.Size(283, 248);
			this.btStatus.TabIndex = 3;
			// 
			// StartForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size(446, 325);
			this.Controls.Add(this.btStatus);
			this.Controls.Add(this.lookForBt);
			this.Controls.Add(this.selectedRectLabel);
			this.Controls.Add(this.testOverlayBtn);
			this.Name = "StartForm";
			this.Text = "VibeYoGames";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnQuit);
			this.Load += new System.EventHandler(this.StartForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button testOverlayBtn;
		private System.Windows.Forms.Label selectedRectLabel;
		private System.Windows.Forms.Button lookForBt;
		private System.Windows.Forms.TextBox btStatus;
	}
}
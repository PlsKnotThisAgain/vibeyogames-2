﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace VibeYoGames.Forms {
	public partial class SelectionOverlayForm : Form {
		private int _x1, _y1, _x2, _y2;
		private bool _draw;
		public static readonly IntPtr HwndTopmost = new IntPtr(-1);
		private const UInt32 SwpNosize = 0x0001;
		private const UInt32 SwpNomove = 0x0002;
		public const UInt32 TopmostFlags = SwpNomove | SwpNosize;
		private readonly SelectionOverlayBackgroundForm _background;
		public delegate void SelectedRect(Rectangle rect);
		public event SelectedRect OnRectSelected;

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);

		public SelectionOverlayForm() {
			StartPosition = FormStartPosition.CenterScreen;
			InitializeComponent();
			_background = new SelectionOverlayBackgroundForm(this);
			SetWindowPos(Handle, HwndTopmost, 0, 0, 0, 0, TopmostFlags);
			_background.Show();
			WindowState = FormWindowState.Maximized;
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			BackColor = Color.Green;
			TransparencyKey = Color.Green;
			Opacity = 1;
		}

		public sealed override Color BackColor
		{
			get => base.BackColor;
			set => base.BackColor = value;
		}

		public void Form1_MouseMove(object sender, MouseEventArgs e)
		{
			if (Disposing || IsDisposed) return;
			if (_draw)
			{
				var oldX = _x2;
				var oldY = _y2;
				_x2 = MousePosition.X - Location.X;
				_y2 = MousePosition.Y - Location.Y;
					if (oldX != _x2 || oldY != _y2)
					{
						using (var g = CreateGraphics())
						{

							var pen = new Pen(Color.Green,2);
							g.DrawRectangle(pen, Math.Min(_x1, oldX), Math.Min(_y1, oldY), Math.Abs(oldX - _x1), Math.Abs(oldY - _y1));
							pen.Dispose();
						}
					}

				using (var g = CreateGraphics())
				{
					var pen = new Pen(Color.FromArgb(255,255,0,0),2);
					g.DrawRectangle(pen, Math.Min(_x1, _x2), Math.Min(_y1, _y2), Math.Abs(_x2 - _x1), Math.Abs(_y2 - _y1));
					pen.Dispose();
				}

			} else {
					using (var g = CreateGraphics())
					{
						var pen = new Pen(Color.Green,2);
					g.DrawRectangle(pen, Math.Min(_x1, _x2), Math.Min(_y1, _y2), Math.Abs(_x2 - _x1), Math.Abs(_y2 - _y1));
					pen.Dispose();
					}
				}
		}

		public void OnKeyPressedSelAre(object sender, KeyEventArgs e) {
			//if (e.KeyCode != Keys.Escape) return;
			OnRectSelected?.Invoke(new Rectangle(0, 0, 0, 0));
			_background.Close();
			Close();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
		}

		public void MouseDownDraw(object sender, MouseEventArgs e) {
			_x1 = MousePosition.X - Location.X;
			_y1 = MousePosition.Y - Location.Y;
			_draw = true;
		}

		public void MouseUpDraw(object sender, MouseEventArgs e) {
			_draw = false;
			OnRectSelected?.Invoke(new Rectangle(Math.Min(_x1, _x2)-1, Math.Min(_y1, _y2)-1, Math.Abs(_x2 - _x1)+1, Math.Abs(_y2 - _y1)+1));
			_background.Close();
			Close();
		}
	}
}

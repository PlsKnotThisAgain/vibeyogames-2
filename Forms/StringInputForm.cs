﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VibeYoGames.Forms {
	public partial class StringInputForm : Form {

		private string result;

		public string Result {
			get => result;
		}
		public StringInputForm() {
			InitializeComponent();
		}

		public StringInputForm(string title, string text, string yesText = "Ok", string noText = "Cancel") {
			InitializeComponent();
			Name = title;
			Text = text;
			button1.Text = yesText;
			button2.Text = noText;
		}

		public sealed override string Text {
			get => base.Text;
			set => base.Text = value;
		}

		private void StringInputForm_Load(object sender, EventArgs e) {

		}

		private void button1_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Yes;
			result = textBox1.Text;
			Close();
		}

		private void button2_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.No;
			Close();
		}
	}
}

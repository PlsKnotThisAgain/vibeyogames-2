﻿using System;
using System.Windows.Forms;

namespace VibeYoGames.Forms {
	public partial class SelectionOverlayBackgroundForm : Form
	{
		private readonly SelectionOverlayForm _caller;
		public SelectionOverlayBackgroundForm(SelectionOverlayForm caller)
		{
			_caller = caller;
			StartPosition = FormStartPosition.CenterScreen;
			InitializeComponent();
			WindowState = FormWindowState.Maximized;
			SelectionOverlayForm.SetWindowPos(Handle, SelectionOverlayForm.HwndTopmost, 0, 0, 0, 0, SelectionOverlayForm.TopmostFlags);

		}

		private void FormBG_Load(object sender, EventArgs e) {

		}

		private void FormBG_KeyDown(object sender, KeyEventArgs e) {
			_caller.OnKeyPressedSelAre(sender, e);
		}

		private void FormBG_MouseDown(object sender, MouseEventArgs e) {
			_caller.MouseDownDraw(sender,e);
		}

		private void FormBG_MouseMove(object sender, MouseEventArgs e) {
			_caller.Form1_MouseMove(sender,e);
		}

		private void FormBG_MouseUp(object sender, MouseEventArgs e) {
			_caller.MouseUpDraw(sender,e);
		}
	}
}

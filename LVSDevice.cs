﻿using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;
using VibeYoGames.Forms;

namespace VibeYoGames {
	class LvsDevice {

		private BluetoothLEDevice _device;
		private StartForm _start;
		private CancelTask _stopVibeTask;
		private GattCharacteristic _vibrateChar;
		private GattCharacteristic _batteryChar;

		private class CancelTask {
			public bool Stop;
		}

		public LvsDevice(BluetoothLEDevice device, StartForm start) {
			_device = device;
			_start = start;
			FindServices();
		}


		/// <summary>
		/// Actually vibrates the Hush
		/// </summary>
		/// <param name="strenght"></param>
		/// <param name="lengthMS"></param>
		public async void Vibrate(int strenght, int lengthMS) {
			strenght = strenght.Clamp(0, 20);
			if (_stopVibeTask != null) {
				_stopVibeTask.Stop = true;
				_stopVibeTask = null;
			}
			var buffer = new DataWriter();
			buffer.WriteString("Vibrate:" + strenght + ";");
			buffer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
			_start.AddStatus("Vibrating at " + strenght);
			await _vibrateChar.WriteValueAsync(buffer.DetachBuffer()).AsTask();
			_stopVibeTask = new CancelTask();
			StopVibeAfterTime(lengthMS, _stopVibeTask);
		}

		/// <summary>
		/// Stops vibration after lengthMS
		/// </summary>
		/// <param name="lengthMS"></param>
		/// <param name="ct"></param>
		private async void StopVibeAfterTime(int lengthMS, CancelTask ct) {
			await Task.Delay(lengthMS);
			if (ct.Stop) {
				return;
			}
			var buffer = new DataWriter();
			buffer.WriteString("Vibrate:0;");
			buffer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
			_start.AddStatus("Stopping vibrations");
			await _vibrateChar.WriteValueAsync(buffer.DetachBuffer()).AsTask();
		}

		/// <summary>
		/// Finds the correct GATT services
		/// </summary>
		private async void FindServices() {
			var services = (await _device.GetGattServicesAsync().AsTask()).Services;
			GattDeviceService service = services[2];
			//6e400001-b5a3-f393-e0a9-e50e24dcca9e
			var chars = (await service.GetCharacteristicsAsync().AsTask()).Characteristics;
			_batteryChar = chars[0];
			_vibrateChar = chars[1];
		}


		public void Dispose() {
			_device.Dispose();
		}
	}
}

﻿using System;
using System.Threading.Tasks;
using Windows.Foundation;

namespace VibeYoGames {
	static class Extensions {

		/// <summary>
		/// Adds a Clamp method to every IComparable
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="val">Value</param>
		/// <param name="min">Minimal value</param>
		/// <param name="max">Maximal value</param>
		/// <returns></returns>
		public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T> {
			if (val.CompareTo(min) < 0) return min;
			else if (val.CompareTo(max) > 0) return max;
			else return val;
		}

		public static bool IsLetter(this char ch) {
			return ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z';
		}

		public static bool IsHex(this char ch) {
			return ch >= 'a' && ch <= 'f' || ch >= 'A' && ch <= 'F' || ch >= '0' && ch <= '9';
		}

		public static bool IsNumber(this char ch) {
			return ch >= '0' && ch <= '9';
		}

		public static bool IsAlphanumeric(this char ch) {
			return ch.IsLetter() || ch.IsNumber();
		}

		public static Task<TResult> AsTask<TResult>(this IAsyncOperation<TResult> operation) {
			var tcs = new TaskCompletionSource<TResult>();
			operation.Completed = delegate {
				switch (operation.Status) {
					case AsyncStatus.Canceled:
						tcs.SetCanceled();
						break;
					case AsyncStatus.Completed:
						tcs.SetResult(operation.GetResults());
						break;
					case AsyncStatus.Error:
						tcs.SetException(operation.ErrorCode);
						break;
					case AsyncStatus.Started:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

			};
			return tcs.Task;
		}
	}

}

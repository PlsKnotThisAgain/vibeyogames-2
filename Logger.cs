﻿using System;
using System.IO;

namespace VibeYoGames {
	public static class Logger {
		private static StreamWriter _swLog;
		private const string SLogFilePath = "vyg.log";

		static Logger() {
			Logger.OpenLogger();
		}

		public static void OpenLogger() {
			Logger._swLog = new StreamWriter(SLogFilePath, false) {AutoFlush = true};
		}

		public static void LogThisLine(string sLogLine) {
			Logger._swLog.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "\t:" + "\t" + sLogLine);
			Logger._swLog.Flush();
		}

		public static void CloseLogger() {
			Logger._swLog.Flush();
			Logger._swLog.Close();
		}
	}
}
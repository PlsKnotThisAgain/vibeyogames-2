## Updates
20190129 - Had to take a break for finals, but I haven't abandoned this. Development should resume during February. I managed to get memory scanner to work so interesting things should be coming.

# VibeYoGames 2

Windows RT got an update! Yay! VYG2 will be a WinForms .NET app instead of UWP.

## OwO What's this?

This is basically a bridge between games and Lovense intimate toys.
Feel the pleasure of dealing 20 damage in Hearthstone!
Get a real connection with Your spaceship in Elite Dangerous!
Just try to survive Dark Souls without your butt getting sore! (NOT IMPLEMENTED YET)

## How is it different from the original?

* I've moved from UWP to WinForms (I just don't like UWP ok)
* The old version could only read logfiles. I've got bigger plans for this one:
 * Logfile reading
 * Memory reading (think read-only cheat engine)
 * *OCR* - This might not be needed with memory reading.

## Installation
 
 I'll provide built versions as soon as the project is usable. 
 You can compile the source through visual studio. Make sure that You have .NET 4.7.1 System.Runtime.dll and newest stable WindowsKit facade in references.

## Supported games
### Working
* Hearthstone
* Elite: Dangerous (*Planned: Fire and hull detection*)
### Planned
* Dark Souls 3 (possibly the remaster and 2 too)
* Overwatch
* StarCitizen
* And more

## Supported devices
Only Hush for now as that's the only one I own. If You own a lovense device You'd like to see a support for, let me know and with Your help I'll add it.